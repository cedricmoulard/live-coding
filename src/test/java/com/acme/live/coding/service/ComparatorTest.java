package com.acme.live.coding.service;

import com.acme.live.coding.exception.InvalidFileException;
import com.acme.live.coding.exception.MissingFileException;
import com.acme.live.coding.model.ComparisonResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ComparatorTest {

    private Comparator comparator;

    @BeforeEach
    void setup() {
        comparator = new Comparator();
    }

    @Test
    void should_return_results() throws IOException {

        // GIVEN
        final String firstFile = "validFile1.txt";
        final String secondFile = "validFile2.txt";
        final ArrayList<ComparisonResult> expected = new ArrayList<>(Collections.singletonList(new ComparisonResult(6, 3, 2)));


        // WHEN
        ArrayList<ComparisonResult> results = comparator.compare(firstFile, secondFile);

        // THEN
        assertEquals(expected, results);

    }

    @Test
    void should_return_empty_results() throws IOException {

        // GIVEN
        final String firstFile = "emptyFile.txt";
        final String secondFile = "validFile2.txt";
        final ArrayList<ComparisonResult> expected = new ArrayList<>();


        // WHEN
        ArrayList<ComparisonResult> results = comparator.compare(firstFile, secondFile);

        // THEN
        assertEquals(expected, results);

    }

    @Test
    void should_throw_exception_with_invalid_file() throws IOException {

        // GIVEN
        final String firstFile = "invalidFile.txt";
        final String secondFile = "validFile2.txt";

        // WHEN

        // THEN
        assertThrows(InvalidFileException.class, () -> comparator.compare(firstFile, secondFile));

    }

    @Test
    void should_throw_exception_with_missing_file() throws IOException {

        // GIVEN
        final String firstFile = "missing.txt";
        final String secondFile = "validFile2.txt";

        // WHEN
        ArrayList<ComparisonResult> results = comparator.compare(firstFile, secondFile);

        // THEN
        assertThrows(MissingFileException.class, () -> comparator.compare(firstFile, secondFile));

    }

}
