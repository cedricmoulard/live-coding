package com.acme.live.coding.exception;

public class InvalidFileException extends Exception{

    public InvalidFileException(String message) {
        super(message);
    }
}

