package com.acme.live.coding.exception;

public class MissingFileException extends Exception{

    public MissingFileException(String message) {
        super(message);
    }
}

