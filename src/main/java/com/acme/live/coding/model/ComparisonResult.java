package com.acme.live.coding.model;

public class ComparisonResult {

    private final Integer number;
    private final Integer lineNumberOnFirstFile;
    private final Integer lineNumberOnSecondFile;

    public ComparisonResult(Integer number, Integer lineNumberOnFirstFile, Integer lineNumberOnSecondFile) {
        this.number = number;
        this.lineNumberOnFirstFile = lineNumberOnFirstFile;
        this.lineNumberOnSecondFile = lineNumberOnSecondFile;
    }

    public Integer getNumber() {
        return number;
    }

    public Integer getLineNumberOnFirstFile() {
        return lineNumberOnFirstFile;
    }

    public Integer getLineNumberOnSecondFile() {
        return lineNumberOnSecondFile;
    }

    @Override
    public String toString() {
        return "ComparisonResult{" +
                "number=" + number +
                ", lineNumberOnFirstFile=" + lineNumberOnFirstFile +
                ", lineNumberOnSecondFile=" + lineNumberOnSecondFile +
                '}';
    }
}
