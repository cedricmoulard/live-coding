package com.acme.live.coding.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.acme.live.coding.model.ComparisonResult;
import org.springframework.stereotype.Service;

@Service
public class Comparator {

    public HashMap<Integer, Integer> fileContent;
    public HashMap<Integer, Integer> fileContent2;

    private ArrayList<ComparisonResult> result;

    public Comparator() {
        fileContent = new HashMap<Integer, Integer>();
        fileContent2 = new HashMap<Integer, Integer>();
    }

    public ArrayList<ComparisonResult> compare(String f1, String f2) throws IOException {
        result = new ArrayList<ComparisonResult>();
        // File are stored in a share place
        final File file = new File(Utils.getFileRepositoryPath() + f1);
        final File file2 = new File(Utils.getFileRepositoryPath() + f1);

        BufferedReader br = null;
        BufferedReader br2 = null;

        int i = 1;
        int j = 0;

        try {
            br = new BufferedReader(new FileReader(file));
        } catch (final FileNotFoundException e) {
            System.out.println("Error opening file 1 " + e.getMessage());
        }

        String st;
        String st2;
        // Iterate over file and put content
        while ((st = br.readLine()) != null) {
            fileContent.put(i, Integer.parseInt(st));
            i++;
            try {
                br2 = new BufferedReader(new FileReader(file));
                while ((st2 = br.readLine()) != null) {
                    System.out.println(Integer.parseInt(st2));
                    fileContent2.put(j, Integer.parseInt(st2));
                    j++;
                }
            } catch (final FileNotFoundException e) {
                System.out.println("Error opening file 1 " + e.getMessage());
            }
        }

        // iterate over content to found the same number
        final int mapSize = fileContent.size();
        for (int k = 0; k < mapSize; k++) {
            for (int l = 0; l < mapSize; l++) {
                if (fileContent.get(k) == (fileContent2).get(l)) {
                    System.out.println("same number found:" + fileContent.get(k));
                    final ComparisonResult comparisonResult = new ComparisonResult(fileContent.get(k), k,l);
                    result.add(comparisonResult);
                }
            }
        }

        // return the result
        return result;

    }
}
