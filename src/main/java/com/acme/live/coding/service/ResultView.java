package com.acme.live.coding.service;

import java.io.IOException;
import java.util.ArrayList;

import com.acme.live.coding.model.ComparisonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResultView {

    @Autowired
    Comparator comparator;

    ArrayList<ComparisonResult> map = null;

    public void findNbr(String s1, String s2) {
        try {
            map = comparator.compare(s1, s2);
        } catch (final IOException e) {
            // Fix the error
        }
        final int listSize = map.size();
        for (int i = 0; i < listSize; i++) {
            map.get(i);
            final Integer nbr = map.get(i).getNumber();
            final Integer l1 = map.get(i).getLineNumberOnFirstFile();
            final Integer l2 = map.get(i).getLineNumberOnSecondFile();
            System.out.println(nbr + " found on line " + l1 + " and " + l2);
        }

    }

}
