package com.acme.live.coding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.acme.live.coding.service.ResultView;

@SpringBootApplication
public class FileComparatorApplication implements CommandLineRunner {

    @Autowired
    ResultView rez;

    public static void main(String[] args) {
        SpringApplication.run(FileComparatorApplication.class, args);
    }

    public void run(String... args) throws Exception {
        if (args.length > 1)
            rez.findNbr(args[0], args[1]);
    }
}